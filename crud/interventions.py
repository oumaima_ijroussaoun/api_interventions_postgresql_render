from datetime import datetime ,date
import datetime
from datetime import datetime
from sqlalchemy.orm import Session, query, session 
from sqlalchemy import column, func 
import pandas as pd
from sqlalchemy import desc
import json
import locale
from typing import List
from sqlalchemy.sql.expression import  case, or_, true,and_
from sqlalchemy.sql.sqltypes import String
from models import interventions as mod
from schemas import interventions as schem
from . import get_db
import requests
from sqlalchemy import func
from typing import Dict




def get_interventions(db: Session = next(get_db())):
    return db.query(mod.SelectedInterventions).filter(mod.SelectedInterventions.inter_status==1).all() 

def get_intervention(id_meter:str,db: Session = next(get_db())):
    return db.query(mod.SelectedInterventions).filter(mod.SelectedInterventions.id_meter==id_meter).order_by(mod.SelectedInterventions.assign_date.desc()).first()


def get_desinstallation(id_foyer,id_meter:str,db: Session = next(get_db())):
    return db.query(mod.SelectedInterventions).filter(mod.SelectedInterventions.id_foyer==id_foyer,mod.SelectedInterventions.id_meter==id_meter).order_by(mod.SelectedInterventions.assign_date.desc()).first()  

#Intervation Rework
def get_intervention_Rework(id_meter:str,id_foyer:int,assign_date:date,db: Session = next(get_db())):
    return db.query(mod.SelectedInterventions).filter(mod.SelectedInterventions.id_meter==id_meter,mod.SelectedInterventions.id_foyer==id_foyer,mod.SelectedInterventions.assign_date==assign_date).order_by(mod.SelectedInterventions.assign_date.desc()).first() 

def get_installation(Numero_recrutement:str,db: Session = next(get_db())):
    return db.query(mod.SelectedInterventions).filter(mod.SelectedInterventions.Numero_recrutement==Numero_recrutement).order_by(mod.SelectedInterventions.assign_date.desc()).first() 

# Updated 08/03/2024
# def get_installation(id_inter:str,db: Session = next(get_db())):
#     return db.query(mod.SelectedInterventions).filter(mod.SelectedInterventions.id_inter==id_inter).order_by(mod.SelectedInterventions.assign_date.desc()).first() 



def get_interventions_by_installer(installer:str,db: Session = next(get_db())):
    return db.query(mod.SelectedInterventions).filter(mod.SelectedInterventions.inter_status==1,mod.SelectedInterventions.installer==installer).all()

#Updated 15/09/2023  ################################################################################################################################
def get_interventions_by_all(installer:str,db: Session = next(get_db())):
    return db.query(mod.SelectedInterventions).filter(mod.SelectedInterventions.inter_status==1).all()
##########################################################################################################################################################
def get_interventions_meters(db: Session = next(get_db())):
    return db.query(mod.SelectedInterventions.id_meter).filter(mod.SelectedInterventions.inter_status==1).all() 



def post_interventions(data:List[schem.CreateInterventions],db: Session = next(get_db())):
    try:
        ############## post interventions

        now = datetime.now()
        day=now.day
        month=now.month
        year=now.year
        now = now.strftime('%Y-%m-%d %H:%M:%S')
        out_dict=[]
        #print(data)
        for elt in data:
            print(elt['nom_installateur'])
            out_dict.append({'id_meter':elt['id_meter'],'id_foyer':elt['id_foy'],'prob':elt['prob'],'installer':elt['nom_installateur']\
                         ,'ville':elt['ville'],'latitude':elt['latitude'],'longitude':elt['longitude'],'adresse':elt['adresse']\
                        ,'nom':elt['nom'],'telephones':elt['telephones'],'inter_type':2,'inter_status':1})
        out = {}
        for d in out_dict:
            if d['id_meter'] in out:
                if isinstance(out[d['id_meter']]['prob'], list):
                    out[d['id_meter']]['prob'].append(d['prob'])
                else:
                    out[d['id_meter']]['prob'] = [out[d['id_meter']]['prob'], d['prob']]
            else:
                out[d['id_meter']] = d
        out = list(out.values())
       
    
        for elt in out:
            if type(elt['prob'])==str:
                elt['prob']=[elt['prob']]

            #updated 16/08/2023
            new_id_inter = str(elt['id_foyer']) + str(day) + str(month) + str(year)
            existing_ids = db.query(mod.SelectedInterventions.id_inter).filter(mod.SelectedInterventions.id_inter == new_id_inter).all()

            if existing_ids:
                new_id_inter =str(new_id_inter)+"2" # Concatenate "2" to the existing id_inter value

            #selected = mod.SelectedInterventions(id_inter=int(str(elt['id_foyer'])+str(day)+str(month)+str(year)),id_foyer=elt['id_foyer'],id_meter=elt['id_meter'],installer=elt['installer'],
            selected = mod.SelectedInterventions(id_inter=new_id_inter,id_foyer=elt['id_foyer'],id_meter=elt['id_meter'],installer=elt['installer'],
            assign_date=now,inter_type=elt['inter_type'],inter_status=elt['inter_status'],ville=elt['ville'],latitude=elt['latitude'],\
                longitude=elt['longitude'],adresse=elt['adresse'],nom=elt['nom'],telephones=elt['telephones'],prob=elt['prob'])

            db.add(selected)
            db.flush()
            db.refresh(selected)
    
        ################commit to database
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()  






########################## Post Desinstallations ###################################################
#Updated 08/09/2023
def post_desinstallations(data:List[schem.CreateInterventions],db: Session = next(get_db())):
    try:

        now = datetime.now()
        day=now.day
        month=now.month
        year=now.year
        now = now.strftime('%Y-%m-%d %H:%M:%S')
        out_dict=[]
        #print(data)
        for elt in data:
            out_dict.append({'id_meter':elt['id_meter'],'id_foyer':elt['id_foy'],'prob':'null','installer':elt['nom_installateur']\
                         ,'ville':elt['ville'],'latitude':elt['latitude'],'longitude':elt['longitude'],'adresse':elt['adresse']\
                        ,'nom':elt['nom'],'telephones':elt['telephones'],'inter_type':3,'inter_status':1})
        out = {}
        for d in out_dict:
            if d['id_meter'] in out:
                if isinstance(out[d['id_meter']]['prob'], list):
                    out[d['id_meter']]['prob'].append(d['prob'])
                else:
                    out[d['id_meter']]['prob'] = [out[d['id_meter']]['prob'], d['prob']]
            else:
                out[d['id_meter']] = d
        out = list(out.values())
       
    
        for elt in out:
            print("elt")
            print(elt)
            if type(elt['prob'])==str:
                elt['prob']=[elt['prob']]

            #updated 16/08/2023
            new_id_inter = str(elt['id_foyer']) + str(day) + str(month) + str(year)
            existing_ids = db.query(mod.SelectedInterventions.id_inter).filter(mod.SelectedInterventions.id_inter == new_id_inter).all()

            if existing_ids:
                new_id_inter =str(new_id_inter)+"2" # Concatenate "2" to the existing id_inter value

            selected = mod.SelectedInterventions(id_inter=new_id_inter,id_foyer=elt['id_foyer'],id_meter=elt['id_meter'],installer=elt['installer'],
            assign_date=now,inter_type=elt['inter_type'],inter_status=elt['inter_status'],ville=elt['ville'],latitude=elt['latitude'],\
                longitude=elt['longitude'],adresse=elt['adresse'],nom=elt['nom'],telephones=elt['telephones'],prob=elt['prob'])
            print("selected")
            print(selected)   
            db.add(selected)
            db.flush()
            db.refresh(selected)
    
        ################ commit to database
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()   





#Updated 08/09/2023
def post_installations(data:List[schem.CreateInterventions],db: Session = next(get_db())):
    try:
        ############## post interventions

        now = datetime.now()
        day=now.day
        month=now.month
        year=now.year
        now = now.strftime('%Y-%m-%d %H:%M:%S')
        out_dict=[]



        for elt in data:
            out_dict.append({'id_meter':'null','id_foyer':0,'prob':'null','installer':elt['Destinataire'],'latitude':0,'longitude':0\
                         ,'ville':elt['VILLE'],'adresse':elt['ADRESSE']\
                        ,'nom':elt['NOM'],'telephones':elt['TELEPHONE'],'inter_type':1,'inter_status':1,'Numero_recrutement':elt['Numero']})
       #Updated 14/09/2023
       # out = {}
        #print("out_dict")
        #print(out_dict)

        #for i in out_dict:
         #   if i['id_meter'] in out:
          #      if isinstance(out[i['id_meter']]['prob'], list):
           #         out[i['id_meter']]['prob'].append(i['prob'])
            #    else:
             #       out[i['id_meter']]['prob'] = [out[i['id_meter']]['prob'], i['prob']]
            #else:
             #   out[i['id_meter']] = i
        #out = list(out.values())
       

       
    


        for elt in out_dict:
            print("elt")
            print(elt)
            if type(elt['prob'])==str:
                elt['prob']=[elt['prob']]

            #updated 16/08/2023
            #new_id_inter = str(elt['telephones']) + str(day) + str(month) + str(year)
            new_id_inter = elt['Numero_recrutement']
            existing_ids = db.query(mod.SelectedInterventions.id_inter).filter(mod.SelectedInterventions.id_inter == new_id_inter).all()

            if existing_ids:
                new_id_inter =str(new_id_inter)+"2" # Concatenate "2" to the existing id_inter value
          

            selected = mod.SelectedInterventions(id_inter=new_id_inter,id_foyer=0,id_meter='null',installer=elt['installer'],
            assign_date=now,inter_type=elt['inter_type'],inter_status=elt['inter_status'],ville=elt['ville'],\
                adresse=elt['adresse'],nom=elt['nom'],telephones=elt['telephones'],prob='null',latitude=elt['latitude'],longitude=elt['longitude'],Numero_recrutement=elt['Numero_recrutement'])
            print("selected")
            print(selected)
            db.add(selected)
            db.flush()
            db.refresh(selected)
    
        ################commit to database
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()  


############################################ Updated 16/04/2024  #############################################################
def get_interventions_by_installer_by_ville(installer: str,ville:str, db: Session = next(get_db())):
    installer_lower = installer.lower()  # Convertit le paramètre en minuscules
    ville_lower = ville.lower()

    return db.query(mod.SelectedInterventions).filter(
        mod.SelectedInterventions.inter_status == 1,
        func.lower(mod.SelectedInterventions.installer) == installer_lower,  # Compare en minuscules
        func.lower(mod.SelectedInterventions.ville) == ville_lower
    ).all()

def get_installations_by_installer_by_ville(installer: str,ville:str, db: Session = next(get_db())):
    installer_lower = installer.lower()  # Convertit le paramètre en minuscules
    ville_lower = ville.lower()

    return db.query(mod.SelectedInterventions).filter(
        mod.SelectedInterventions.inter_status == 1,
        mod.SelectedInterventions.inter_type == 1,
        func.lower(mod.SelectedInterventions.installer) == installer_lower,  # Compare en minuscules
        func.lower(mod.SelectedInterventions.ville) == ville_lower
    ).all()

def get_reparations_by_installer_by_ville(installer: str,ville:str, db: Session = next(get_db())):
    installer_lower = installer.lower()  # Convertit le paramètre en minuscules
    ville_lower = ville.lower()

    return db.query(mod.SelectedInterventions).filter(
        mod.SelectedInterventions.inter_status == 1,
        mod.SelectedInterventions.inter_type == 2,
        func.lower(mod.SelectedInterventions.installer) == installer_lower,  # Compare en minuscules
        func.lower(mod.SelectedInterventions.ville) == ville_lower
    ).all()

def get_desinstallations_by_installer_by_ville(installer: str,ville:str, db: Session = next(get_db())):
    installer_lower = installer.lower()  # Convertit le paramètre en minuscules
    ville_lower = ville.lower()

    return db.query(mod.SelectedInterventions).filter(
        mod.SelectedInterventions.inter_status == 1,
        mod.SelectedInterventions.inter_type == 3,
        func.lower(mod.SelectedInterventions.installer) == installer_lower,  # Compare en minuscules
        func.lower(mod.SelectedInterventions.ville) == ville_lower
    ).all()












######################################################################################################################################
def get_interventions_by_installer_type(installer: str, type_int: int, db: Session = next(get_db())):
    installer_lower = installer.lower()  # Convertit le paramètre en minuscules

    return db.query(mod.SelectedInterventions).filter(
        mod.SelectedInterventions.inter_status == 1,
        func.lower(mod.SelectedInterventions.installer) == installer_lower,  # Compare en minuscules
        mod.SelectedInterventions.inter_type == type_int
    ).all()

## Updated 15/09/2023 ############################################################################################################
def get_interventions_by_all_type(installer:str,type_int:int,db: Session = next(get_db())):
    return db.query(mod.SelectedInterventions).filter(mod.SelectedInterventions.inter_status==1,mod.SelectedInterventions.inter_type==type_int).all()
####################################################################################################""""


###################### Post Validated Interventions ##############################################

def post_validated_interventions(data:List[schem.ValidatedInterventions],db: Session = next(get_db())):
    try:


        ############## post interventions
        for elt in data:
            print(elt)

            selected = mod.ValidatedInterventions(statut_intervention="validated",id_inter=elt['id_inter'],
            Type_intervention =elt['Type_intervention'] if 'Type_intervention' in elt else 'null',
            id_foyer=0 if  elt['Type_intervention']=="Installation" else elt['id_foyer'],
            id_meter=elt['id_meter'] if 'id_meter' in elt else 'null' ,
            new_id_meter=elt['new_id_meter'] if 'new_id_meter' in elt else 'null',
            #Nouveau_id_meter=elt['Nouveau_id_meter'] if 'Nouveau_id_meter' in elt else 'null',
            installer=elt['installer'],
            assign_date=elt['assign_date'] if 'assign_date' in elt else 'null',
            interv_date=elt['interv_date'] if 'interv_date' in elt else 'null',
            #Date_intervention=elt['Date_intervention'],
            distance=elt['distance'],hotel=elt['hotel'],
            parking=elt['parking'],
            Repas=elt['Repas'],
            accessoires=elt['Frais_autres_accessoires'],
            Notes_Frais=int(elt['hotel']) +int(elt['parking'])+int(elt['Repas']) +int(elt['Frais_autres_accessoires']),
            Etat_du_foyer=elt['Etat_du_foyer'],
            #Updated 18/08/2023
            Intervention_telephonique=elt['Intervention_telephonique']  if 'Intervention_telephonique' in elt else 'null',
            longitude=elt['longitude'] if 'longitude' in elt else 'null',
            latitude=elt['latitude'] if 'latitude' in elt else 'null' ,
            Type_audio=elt['Type_audio'] if 'Type_audio' in elt else 'null',
            Type_tv=elt['Type_tv'] if 'Type_tv' in elt else 'null',
            Diagnostic_intervention=elt['Diagnostic_intervention'] if 'Diagnostic_intervention' in elt else 'null')
            db.add(selected)
            db.flush()
            db.refresh(selected)
        
        ################commit to database
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'inserted successfuly'  

    finally:
        db.close()  



def post_Business_Validation_Interventions(data:List[schem.BusinessValidationInterventions],statut_intervention:str,db: Session = next(get_db())):
    try:


        ############## post interventions
        for elt in data:
            print(elt)
            
            selected = mod.BusinessValidationInterventions(statut_intervention=statut_intervention,
            id_inter=elt['id_inter'],
            Type_intervention =elt['Type_intervention'] if 'Type_intervention' in elt else 'null',
            id_foyer=0 if  elt['Type_intervention']=="Installation" else elt['id_foyer'],
            id_meter=elt['id_meter'] if 'id_meter' in elt else 'null' ,
            new_id_meter=elt['new_id_meter'] if 'new_id_meter' in elt else 'null',
            installer=elt['installer'],
            assign_date=elt['assign_date'],
            interv_date=elt['interv_date'],
            Etat_du_foyer=elt['Etat_du_foyer'],
            #Updated 18/08/2023
            Intervention_telephonique=elt['Intervention_telephonique']  if 'Intervention_telephonique' in elt else 'null',
            longitude=elt['longitude'] if 'longitude' in elt else 'null',
            latitude=elt['latitude'] if 'latitude' in elt else 'null' ,
            Type_audio=elt['Type_audio'] if 'Type_audio' in elt else 'null',
            Type_tv=elt['Type_tv'] if 'Type_tv' in elt else 'null',
            Diagnostic_intervention=elt['Diagnostic_intervention'] if 'Diagnostic_intervention' in elt else 'null')
            


            print("******************************************selected")
            print(selected)
            db.add(selected)
            db.flush()
            db.refresh(selected)

            if(statut_intervention=="Rework"):
                stored_intev = get_intervention_Rework(id_meter=elt['id_meter'],id_foyer=elt['id_foyer'],assign_date=elt['assign_date'],db=db)
                print("######################################################################################")
                print(data)
                print("stored_intev")
                print(stored_intev)
                setattr(stored_intev, 'inter_status', 1)
                
        ################commit to database
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'inserted successfully'  

    finally:
        db.close() 


###################### Get Validated Interventions #########################
def get_validated_interventions(db: Session = next(get_db())):
    return db.query(mod.ValidatedInterventions).all()


###################### Get Validated Interventions by date #########################
def get_validated_interventions_by_date(date_deb:date,date_fin:date,db: Session = next(get_db())):
    return db.query(mod.ValidatedInterventions).filter(mod.ValidatedInterventions.interv_date.between(date_deb, date_fin)).all()



def get_Business_Validation_Interventions(db: Session = next(get_db())):
    return db.query(mod.BusinessValidationInterventions).filter(mod.BusinessValidationInterventions.statut_intervention=="Validated").all()

def get_Business_Validation_Interventions_by_date(date_deb:date,date_fin:date,db: Session = next(get_db())):
    return db.query(mod.BusinessValidationInterventions).filter(mod.BusinessValidationInterventions.statut_intervention=="Validated",
    mod.BusinessValidationInterventions.interv_date.between(date_deb, date_fin)).all()

#Get Validated Interventions (Validated and rework )
def get_Business_Validation_Interventions_Validated_Rework(db: Session = next(get_db())):
    return db.query(mod.BusinessValidationInterventions).all()


#Get Validated Interventions (Validated and rework ) By date
def get_Business_Validation_Interventions_Validated_Rework_by_date(date_deb:date,date_fin:date,db: Session = next(get_db())):
    return db.query(mod.BusinessValidationInterventions).filter(mod.BusinessValidationInterventions.interv_date.between(date_deb, date_fin)).all()



###################### Get Validated Interventions By Installer #########################
def get_validated_interventions_by_installer(installer:str,date_deb:date,date_fin:date,db: Session = next(get_db())):
    return db.query(mod.ValidatedInterventions).filter(mod.ValidatedInterventions.installer==installer,
    mod.ValidatedInterventions.interv_date.between(date_deb, date_fin)).all()






def get_validated_interventions_by_installer_process(installer: str, date_deb: date, date_fin: date, db: Session = next(get_db())):
    records = db.query(mod.ValidatedInterventions).filter(
        mod.ValidatedInterventions.installer == installer,
        mod.ValidatedInterventions.interv_date.between(date_deb, date_fin)
    ).all()

    max_values_by_date = {}

    # Trouver les valeurs maximales pour Repas, parking et hotel pour chaque date d'intervention.
    for record in records:
        interv_date = record.interv_date
        if interv_date not in max_values_by_date:
            max_values_by_date[interv_date] = {
                "Repas": int(record.Repas),
                "parking": int(record.parking),
                "hotel": int(record.hotel)
            }
        else:
            max_values_by_date[interv_date]["Repas"] = max(max_values_by_date[interv_date]["Repas"], int(record.Repas))
            max_values_by_date[interv_date]["parking"] = max(max_values_by_date[interv_date]["parking"], int(record.parking))
            max_values_by_date[interv_date]["hotel"] = max(max_values_by_date[interv_date]["hotel"], int(record.hotel))
    
    updated_records = []
    updated_dates = set()
    for record in records:
        interv_date = record.interv_date
        if interv_date not in updated_dates:
            record.Repas = str(max_values_by_date[interv_date]["Repas"])
            record.parking = str(max_values_by_date[interv_date]["parking"])
            record.hotel = str(max_values_by_date[interv_date]["hotel"])
            updated_dates.add(interv_date)
        else:
            record.Repas = "0"
            record.parking = "0"
            record.hotel = "0"
        updated_records.append(record)

    return updated_records

   





###################### Get Global_Repondant_Foyers By Installer #########################
def Global_Repondant_Foyers(installer: str, date_deb: date, date_fin: date, nbr_jour_apres_interv:int,db: Session = next(get_db())):
    res = db.query(mod.BusinessValidationInterventions).filter(
        mod.BusinessValidationInterventions.installer == installer,mod.BusinessValidationInterventions.statut_intervention == "Validated",
        mod.BusinessValidationInterventions.interv_date.between(date_deb, date_fin)
    ).all()
    
    data_list = []
    for item in res:
        id_inter=item.id_inter
        id_foyer = item.id_foyer
        date_interv = item.interv_date
        id_meter=item.id_meter
        #Nouveau_id_meter=item.Nouveau_id_meter
        new_id_meter=item.new_id_meter
        Type_intervention=item.Type_intervention
        #Updated 18/08/2023
        Intervention_telephonique = item.Intervention_telephonique
        print("Type_intervention")
        print(Type_intervention)
        #date_interv = datetime.datetime.strptime(item.Date_intervention, "%Y-%m-%d").date()
        data_list.append({"id_foyer": id_foyer,"id_inter": id_inter, "date": date_interv,"id_meter" : id_meter,"new_id_meter":new_id_meter,"Type_intervention":Type_intervention,"Intervention_telephonique":Intervention_telephonique})
   
        
    
    
    results = []
    for info in data_list:
        url = "http://192.168.10.85:8002/audiences/global_repondant/get"
        params = {
            'date_interv': info["date"],
            'id_inter':info["id_inter"],
            'id_foyer': info["id_foyer"],
            'param': nbr_jour_apres_interv,
            'id_meter':info["id_meter"],
            'new_id_meter':info["new_id_meter"],
            'Type_intervention':info["Type_intervention"],
            'Intervention_telephonique':info["Intervention_telephonique"],
        }
        response = requests.get(url, params=params)
        
        data = response.json()
        results.append(data)
    print("##############################results#########################")
    print(results)
        
    return (results)






###################### Get Global_Repondant_Foyers By Installer and by intervention type #########################
def Global_Repondant_Foyers_By_Type_intervention(installer: str, date_deb: date, date_fin: date, nbr_jour_apres_interv:int,db: Session = next(get_db())):
    res = db.query(mod.BusinessValidationInterventions).filter(
        mod.BusinessValidationInterventions.installer == installer,mod.BusinessValidationInterventions.statut_intervention == "Validated",
        mod.BusinessValidationInterventions.interv_date.between(date_deb, date_fin)
    ).all()
    
    data_list = []
    for item in res:
        id_inter=item.id_inter
        id_foyer = item.id_foyer
        interv_date = item.interv_date
        id_meter=item.id_meter
        new_id_meter=item.new_id_meter
        Type_intervention=item.Type_intervention
        #Updated 18/08/2023
        Intervention_telephonique = item.Intervention_telephonique
        #date_interv = datetime.datetime.strptime(item.Date_intervention, "%Y-%m-%d").date()
        data_list.append({"id_foyer": id_foyer,"id_inter": id_inter, "date": interv_date,"id_meter" : id_meter,"new_id_meter":new_id_meter,"Type_intervention":Type_intervention,"Intervention_telephonique":Intervention_telephonique})
        
        
        
    
    
    results = []
    for info in data_list:
        url = "http://192.168.10.85:8002/audiences/global_repondant/get"
        params = {
            'date_interv': info["date"],
            'id_inter':info["id_inter"],
            'id_foyer': info["id_foyer"],
            'param': nbr_jour_apres_interv,
            'id_meter':id_meter,
            'new_id_meter':new_id_meter,
            'Type_intervention':Type_intervention,
            'Intervention_telephonique':info["Intervention_telephonique"],

        }
        response = requests.get(url, params=params)
        data = response.json()
        results.append(data)
        print("results")
        print(results)



        totals = {}  # Dictionnaire pour stocker les totaux des Nbr_OK par type d'intervention

        for item in results:
            type_intervention = item["Type_intervention"]
            nbr_ok = item["Nbr_OK"]
            
            if type_intervention in totals:
                totals[type_intervention] += nbr_ok
            else:
                totals[type_intervention] = nbr_ok

        print(totals)
    return (totals)

    
    
    


###################### Update  Interventions #########################
def put_Business_Validation_Interventions(data:schem.BusinessValidationInterventions,db: Session = next(get_db())):
    intervention_to_update= db.query(mod.BusinessValidationInterventions).filter(mod.BusinessValidationInterventions.id_inter == data["id_inter"]).first()
    
    intervention_to_update.statut_intervention =data["statut_intervention"]
    intervention_to_update.id_foyer =data["id_foyer"]
    intervention_to_update.id_meter =data["id_meter"]
    intervention_to_update.new_id_meter =data["new_id_meter"]
    intervention_to_update.interv_date = data["interv_date"]
    intervention_to_update.assign_date = data["assign_date"]
    intervention_to_update.installer = data["installer"]
    intervention_to_update.Etat_du_foyer = data["Etat_du_foyer"]
    #Updated 18/08/2023
    intervention_to_update.Intervention_telephonique = data["Intervention_telephonique"]
    intervention_to_update.Nouveau_id_meter = data["Nouveau_id_meter"]
    intervention_to_update.Type_audio = data["Type_audio"]
    intervention_to_update.Type_intervention = data["Type_intervention"]
    intervention_to_update.Type_tv = data["Type_tv"]
    intervention_to_update.Diagnostic_intervention = data["Diagnostic_intervention"]
    intervention_to_update.longitude = data["longitude"]
    intervention_to_update.latitude = data["latitude"]
    intervention_to_update.distance = data["distance"]
    intervention_to_update.parking = data["parking"]
    intervention_to_update.Repas = data["Repas"]
    intervention_to_update.hotel = data["hotel"]
    intervention_to_update.Notes_Frais=data['Notes_Frais']
   



    db.commit()
    print(intervention_to_update.distance)
    print(intervention_to_update.Repas)
    print(intervention_to_update.parking)
    print(intervention_to_update.hotel)
 
    return intervention_to_update
    


###################### Post Primes #########################

def post_primes(data:List[schem.Primes],installer:str,db: Session = next(get_db())):
    try:


        ############## post primes
        for elt in data:
            print(elt)

            selected = mod.Primes(id_inter=elt['id_inter'],Foyer_ID=int(elt['Foyer_ID']),id_meter=elt['id_meter'],new_id_meter=elt['new_id_meter'],installer=installer,
            Date_intervention=elt['Date_intervention'],Type_intervention=elt['Type_intervention'],Intervention_telephonique=elt['Intervention_telephonique'],Jour=elt['Jour'],coefficient=elt['coefficient'],prix_coeff=elt['prix_coeff'],Nbr_OK=elt['Nbr_OK'],validation=elt['validation'],validation_finale=elt['validation_finale'])
            db.add(selected)
            db.flush()
            db.refresh(selected)
        
        ################commit to database 
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'inserted successfuly'  

    finally:
        db.close()  





###################### Get Primes #########################


def get_primes(db: Session = next(get_db())):
    return db.query(mod.Primes).all()





###################### Get Primes by Installer #########################
def get_primes_by_installer(installer:str,date_deb:date,date_fin:date,db: Session = next(get_db())):
    return db.query(mod.Primes).filter(
        mod.Primes.installer == installer,
        mod.Primes.Date_intervention.between(date_deb, date_fin)).all()


###################### Get Primes by Type d'intervention #########################
def get_primes_by_type_intervention(date_deb: date, date_fin: date, db: Session = next(get_db())):
    count_by_type = db.query(mod.Primes.Type_intervention,mod.Primes.installer, func.count()).filter(
        mod.Primes.Date_intervention.between(date_deb, date_fin)
    ).group_by(mod.Primes.Type_intervention,mod.Primes.installer).all()
    
    result = [{"type_intervention": type_intervention, "installer":installer,"count": count} for type_intervention,installer, count in count_by_type]
    return result

###################### Get Primes by Type d'intervention and Installer #########################
def get_primes_by_type_intervention_installer(installer:str,date_deb: date, date_fin: date, db: Session = next(get_db())):
    count_by_type = db.query(mod.Primes.Type_intervention, func.count()).filter( mod.Primes.installer == installer,
        mod.Primes.Date_intervention.between(date_deb, date_fin)
    ).group_by(mod.Primes.Type_intervention,).all()
    
    result = [{"type_intervention": type_intervention, "count": count} for type_intervention, count in count_by_type]
    return result
    






############################################dahshboard flutter############################################################"

def get_count_inter_by_installer(installer:str,db: Session = next(get_db())):
    installer_lower = installer.lower()  # Convertit le paramètre en minuscules
   

    out={}
    count =db.query(mod.SelectedInterventions.inter_type,func.count(mod.SelectedInterventions.inter_type).label('inter_type_count')).filter(mod.SelectedInterventions.inter_status==1,func.lower(mod.SelectedInterventions.installer) == installer_lower,).group_by(mod.SelectedInterventions.inter_type).all()
    # for x in count:
    #     out[f'{x["inter_type"]}']=x["inter_type_count"]
    out={x["inter_type"]:x["inter_type_count"] for x in count}
    
    return out

############################################ Updated 15/09/2023 ############################################################"
def get_count_inter_by_all(installer:str,db: Session = next(get_db())):
    out={}
    count =db.query(mod.SelectedInterventions.inter_type,func.count(mod.SelectedInterventions.inter_type).label('inter_type_count')).filter(mod.SelectedInterventions.inter_status==1).group_by(mod.SelectedInterventions.inter_type).all()
    # for x in count:
    #     out[f'{x["inter_type"]}']=x["inter_type_count"]
    out={x["inter_type"]:x["inter_type_count"] for x in count}
    
    return out
###############################################################################################################################""""

def get_count_inter_by_installer_by_vile(installer: str, db: Session = next(get_db())):
    out = {}
    installer_lower = installer.lower()
    
    # Utiliser lower() pour normaliser les noms de ville
    count = db.query(func.lower(mod.SelectedInterventions.ville).label('ville_lower'), func.count(mod.SelectedInterventions.ville).label('ville_count')).filter(mod.SelectedInterventions.inter_status == 1, func.lower(mod.SelectedInterventions.installer) == installer_lower).group_by(func.lower(mod.SelectedInterventions.ville)).all()
    
    # Construire la sortie en utilisant les noms de villes normalisés
    out = [f'{x["ville_lower"]}+{x["ville_count"]}' for x in count]
    
    return out


# def get_count_inter_by_installer_by_vile(installer:str,db: Session = next(get_db())):
#     out={}
#     installer_lower = installer.lower()
#     count =db.query(mod.SelectedInterventions.ville,func.count(mod.SelectedInterventions.ville).label('ville_count')).filter(mod.SelectedInterventions.inter_status==1,func.lower(mod.SelectedInterventions.installer) == installer_lower).group_by(mod.SelectedInterventions.ville).all()
#     # for x in count:
#     #     out[f'{x["inter_type"]}']=x["inter_type_count"]
#     out=[f'{x["ville"]}+{x["ville_count"]}' for x in count]
    
#     return out


############################################ Updated 15/09/2023 ############################################################"
def get_count_inter_by_all_by_vile(installer:str,db: Session = next(get_db())):
    out={}
    count =db.query(mod.SelectedInterventions.ville,func.count(mod.SelectedInterventions.ville).label('ville_count')).filter(mod.SelectedInterventions.inter_status==1).group_by(mod.SelectedInterventions.ville).all()
    # for x in count:
    #     out[f'{x["inter_type"]}']=x["inter_type_count"]
    out=[f'{x["ville"]}+{x["ville_count"]}' for x in count]
    
    return out
###############################################################################################################################""""



def get_count_stats_by_installer(installer:str,db: Session = next(get_db())):
    out={}
    count =db.query(mod.SelectedInterventions.inter_status,func.count(mod.SelectedInterventions.inter_status).label('inter_status_count')).filter(mod.SelectedInterventions.installer==installer).group_by(mod.SelectedInterventions.inter_status).all()
    # for x in count:
    #     out[f'{x["inter_type"]}']=x["inter_type_count"]
    out={x["inter_status"]:x["inter_status_count"] for x in count}
    
    return out

############################################ Updated 15/09/2023 ############################################################"
def get_count_stats_by_all(installer:str,db: Session = next(get_db())):
    out={}
    count =db.query(mod.SelectedInterventions.inter_status,func.count(mod.SelectedInterventions.inter_status).label('inter_status_count')).group_by(mod.SelectedInterventions.inter_status).all()
    # for x in count:
    #     out[f'{x["inter_type"]}']=x["inter_type_count"]
    out={x["inter_status"]:x["inter_status_count"] for x in count}
    
    return out
###############################################################################################################################""""



