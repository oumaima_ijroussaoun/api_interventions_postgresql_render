from fastapi import FastAPI
from sqlalchemy import orm
from routers import include_route
from routers import forms
from routers import gpanel

# from routers import init_workspace
from routers import interventions
from sqlalchemy.dialects.postgresql import asyncpg
from fastapi.middleware.cors import CORSMiddleware

# from models import init_db_from_models
from database import Base, engine

# from . import cros_app

# init_db_from_models(Base,engine,orm)
app=FastAPI(prefix='/')

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE", "OPTIONS"],
    allow_headers=["*"],
)

# port_app(app)
# cros_app(app)
include_route(app)
