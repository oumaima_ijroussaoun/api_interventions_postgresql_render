from fastapi import FastAPI
from sqlalchemy import orm
from routers import include_route
from routers import forms
from routers import init_workspace
from routers import interventions

from models import init_db_from_models
from database import Base, engine

from . import cros_app

init_db_from_models(Base,engine,orm)
app=FastAPI(prefix='/')

# port_app(app)
cros_app(app)
include_route(app)