from ..database import SessionLocal

class MySuperContextManager:
    def __init__(self):
        self.db = SessionLocal()

    def __enter__(self):
        return self.db

    def __exit__(self, exc_type, exc_value, traceback):
        self.db.close()

#seesion 
def get_db():
    with MySuperContextManager() as db:
        try:
            yield db
            db.commit()
        except:
            db.rollback()
            raise   
        finally:
	        db.close()

