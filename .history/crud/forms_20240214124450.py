from pydoc import importfile
from sqlalchemy.orm import Session, query, session 
from sqlalchemy import column, func 
import pandas as pd
import json
from sqlalchemy import desc
import json
from sqlalchemy.sql.expression import  case, or_, true,and_
from sqlalchemy.sql.sqltypes import String

from models import forms as mod
from models import interventions as mod_inter
from schemas import forms as schem
from . import interventions as crud_inter

from models import interventions as intervmod
from schemas import interventions as intervschem

import datetime

from datetime import date



from . import get_db


def post_install_form(data:schem.InstallForm,db: Session = next(get_db())):
    print("data")
    print(data)
    try:
        inst=mod.InstallForm(id_meter=data.id_meter,interv_date=data.interv_date.date(),Numero_recrutement=data.Numero_recrutement,assign_date=data.assign_date.date(),installer=data.installer,form_data=data.form_data)
        db.add(inst)
        db.flush() 
        db.refresh(inst)
        stored_intev = crud_inter.get_installation(id_inter=data.Numero_recrutement,db=db)
        setattr(stored_intev, 'inter_status', 2)
        db.flush()

    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()  

def post_uninstall_form(data:schem.UninstallForm,db: Session = next(get_db())):
    print("data")
    print(data)
    try:
        uninst=mod.UninstallForm(id_foyer=data.id_foyer,id_meter=data.id_meter,interv_date=data.interv_date.date(),assign_date=data.assign_date.date(),installer=data.installer,form_data=data.form_data)
        db.add(uninst)
        db.flush() 
        db.refresh(uninst)
        stored_intev = crud_inter.get_intervention(id_meter=data.id_meter,db=db)
        print("######################################################################################")
        print(data)
        print("stored_intev")
        print(stored_intev)
        setattr(stored_intev, 'inter_status', 2)
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close()  

def post_repair_form(data:schem.RepareForm,db: Session = next(get_db())):
    try:
        rep=mod.RepareForm(id_foyer=data.id_foyer,old_id_meter=data.old_id_meter,new_id_meter=data.new_id_meter,interv_date=data.interv_date.date(),assign_date=data.assign_date.date(),installer=data.installer,form_data=data.form_data)
        db.add(rep)
        db.flush() 
        db.refresh(rep)
        stored_intev = crud_inter.get_intervention(id_meter=data.old_id_meter,db=db)
        setattr(stored_intev, 'inter_status', 2)
    except:
        db.rollback()
          
        raise 
    else:
        db.commit()
        return 'created successfuly'  

    finally:
        db.close() 

#Updated by Oumaima    
def get_repair_form(db: Session = next(get_db())):
    repair=db.query(mod.RepareForm.assign_date,mod.RepareForm.id_foyer,mod.RepareForm.id_inter,mod.RepareForm.installer,
    mod.RepareForm.interv_date,mod.RepareForm.new_id_meter,mod.RepareForm.old_id_meter,mod.RepareForm.form_data).all()
    df=pd.DataFrame(repair)
    df=df.to_dict("records")
    #df_json=pd.json_normalize(df["form_data"])
    #print(df_json)
    #df_concat=df.join(df_json)
    #print(df_concat)
    def flatten_list(lst):
        return [
            {**item, **item.pop('form_data')}
            if 'form_data' in item
            else item
            for item in lst
        ]
    d=flatten_list(df)
    d=pd.DataFrame(d)
    d=d.loc[:,~d.columns.duplicated()]
    #d=d[['assign_date','id_foyer','id_inter','installer','interv_date','new_id_meter','old_id_meter',"Date d'intervention",'Etat du foyer','Nouveau id_meter','Type audio','Type tv','longitude','latitude',
    #'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires "]]
    #Updated 18/08/2023
    d=d[['assign_date','id_foyer','id_inter','installer','interv_date','new_id_meter','old_id_meter',"Date d'intervention",'Etat du foyer','Nouveau id_meter','Type audio','Type tv','longitude','latitude',
    'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires ",'Intervention telephonique']]
    d["Type_intervention"]="Réparation"
    d["Parking"]=d["Parking"].astype(float)
    d["Hotel"]=d["Hotel"].astype(float)
    d["Repas"]=d["Repas"].astype(float)
    d["Frais d'achat d'autres accessoires "]=d["Frais d'achat d'autres accessoires "].astype(float)
    d["Notes_Frais"]=d["Hotel"]+d["Repas"]+d["Parking"]+d["Frais d'achat d'autres accessoires "]
    #d["Distance"]=str(d["Distance"])
    d.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Nouveau id_meter':'Nouveau_id_meter','Distance':'distance','Hotel':'hotel','Parking':'parking',
    'Type audio':'Type_audio','Type tv':'Type_tv','Diagnostic intervention':'Diagnostic_intervention',"Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Intervention telephonique':'Intervention_telephonique'
    },inplace=True)

    d.fillna(0,inplace=True)
    d=d.to_dict("records")
    print(d)
    
    max_values_by_date_and_installer = {}

    for record in d:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in max_values_by_date_and_installer:
            max_values_by_date_and_installer[key] = {
                "Repas": float(record["Repas"]),
                "parking": float(record["parking"]),
                "hotel": float(record["hotel"])
            }
        else:
            max_values_by_date_and_installer[key]["Repas"] = max(max_values_by_date_and_installer[key]["Repas"], float(record["Repas"]))
            max_values_by_date_and_installer[key]["parking"] = max(max_values_by_date_and_installer[key]["parking"], float(record["parking"]))
            max_values_by_date_and_installer[key]["hotel"] = max(max_values_by_date_and_installer[key]["hotel"], float(record["hotel"]))

    # Mettre à jour les enregistrements avec les valeurs maximales pour chaque date d'intervention et chaque installateur.
    updated_records = []
    updated_keys = set()
    for record in d:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in updated_keys:
            record["Repas"] = float(max_values_by_date_and_installer[key]["Repas"])
            record["parking"] = float(max_values_by_date_and_installer[key]["parking"])
            record["hotel"] = float(max_values_by_date_and_installer[key]["hotel"])
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])
        
            updated_keys.add(key)
        else:
            record["Repas"] = 0
            record["parking"] = 0
            record["hotel"] = 0
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])
        updated_records.append(record)

    return updated_records



#Updated by Oumaima    
def get_repair_form_before_process(db: Session = next(get_db())):

    repair=db.query(mod.RepareForm.assign_date,mod.RepareForm.id_foyer,mod.RepareForm.id_inter,mod.RepareForm.installer,
    mod.RepareForm.interv_date,mod.RepareForm.new_id_meter,mod.RepareForm.old_id_meter,mod.RepareForm.form_data).all()
    df=pd.DataFrame(repair)
    df=df.to_dict("records")
    #df_json=pd.json_normalize(df["form_data"])
    #print(df_json)
    #df_concat=df.join(df_json)
    #print(df_concat)
    def flatten_list(lst):
        return [
            {**item, **item.pop('form_data')}
            if 'form_data' in item
            else item
            for item in lst
        ]
    d=flatten_list(df)
    d=pd.DataFrame(d)
    d=d.loc[:,~d.columns.duplicated()]
    d=d[['assign_date','id_foyer','id_inter','installer','interv_date','new_id_meter','old_id_meter',"Date d'intervention",'Etat du foyer','Nouveau id_meter','Type audio','Type tv','longitude','latitude',
    'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires ",'Intervention telephonique']]
    d["Type_intervention"]="Réparation"
    d["Parking"]=d["Parking"].astype(float)
    d["Hotel"]=d["Hotel"].astype(float)
    d["Repas"]=d["Repas"].astype(float)
    d["Frais d'achat d'autres accessoires "]=d["Frais d'achat d'autres accessoires "].astype(float)
    d["Notes_Frais"]=d["Hotel"]+d["Repas"]+d["Parking"]+d["Frais d'achat d'autres accessoires "]
    #d["Distance"]=str(d["Distance"])
    d.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Nouveau id_meter':'Nouveau_id_meter','Distance':'distance','Hotel':'hotel','Parking':'parking',
    'Type audio':'Type_audio','Type tv':'Type_tv','Diagnostic intervention':'Diagnostic_intervention',"Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Intervention telephonique':'Intervention_telephonique'
    },inplace=True)

    d.fillna(0,inplace=True)
    d=d.to_dict("records")
    print(d)
    return d










def get_install_form(db: Session = next(get_db())):
    install=db.query(mod.InstallForm.assign_date,mod.InstallForm.id_inter,mod.InstallForm.id_meter,
    mod.InstallForm.installer,mod.InstallForm.interv_date,mod.InstallForm.form_data).all()  
    df=pd.DataFrame(install)
    df=df.to_dict("records")
    def flatten_list(lst):
        return [
            {**item, **item.pop('form_data')}
            if 'form_data' in item
            else item
            for item in lst
        ]
    d=flatten_list(df)
    d=pd.DataFrame(d)
    d=d.loc[:,~d.columns.duplicated()]
    d=d[['assign_date','id_inter','id_meter','installer','interv_date',"Date d'intervention",'Etat du foyer','longitude','latitude','Type audio','Type tv',
    'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires "]]
    d["Type_intervention"]="Installation"
    print(d)
    d["Parking"]=d["Parking"].astype(float)
    d["Hotel"]=d["Hotel"].astype(float)
    d["Repas"]=d["Repas"].astype(float)
    d["Frais d'achat d'autres accessoires "]=d["Frais d'achat d'autres accessoires "].astype(float)
    d["Notes_Frais"]=d["Hotel"]+d["Repas"]+d["Parking"]+d["Frais d'achat d'autres accessoires "]
    d.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Distance':'distance','Hotel':'hotel','Parking':'parking','Type audio':'Type_audio','Type tv':'Type_tv',
    "Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Diagnostic intervention':'Diagnostic_intervention'
    },inplace=True)
    d.fillna(0,inplace=True)
    d=d.to_dict("records")
    print(d)
    max_values_by_date_and_installer = {}

    for record in d:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in max_values_by_date_and_installer:
            max_values_by_date_and_installer[key] = {
                "Repas": float(record["Repas"]),
                "parking": float(record["parking"]),
                "hotel": float(record["hotel"])
            }
        else:
            max_values_by_date_and_installer[key]["Repas"] = max(max_values_by_date_and_installer[key]["Repas"], float(record["Repas"]))
            max_values_by_date_and_installer[key]["parking"] = max(max_values_by_date_and_installer[key]["parking"], float(record["parking"]))
            max_values_by_date_and_installer[key]["hotel"] = max(max_values_by_date_and_installer[key]["hotel"], float(record["hotel"]))

    # Mettre à jour les enregistrements avec les valeurs maximales pour chaque date d'intervention et chaque installateur.
    updated_records = []
    updated_keys = set()
    for record in d:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in updated_keys:
            record["Repas"] = float(max_values_by_date_and_installer[key]["Repas"])
            record["parking"] = float(max_values_by_date_and_installer[key]["parking"])
            record["hotel"] = float(max_values_by_date_and_installer[key]["hotel"])
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])

            updated_keys.add(key)
        else:
            record["Repas"] = 0
            record["parking"] = 0
            record["hotel"] = 0
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])

        updated_records.append(record)

    return updated_records



def get_install_form_before_process(db: Session = next(get_db())):
    install=db.query(mod.InstallForm.assign_date,mod.InstallForm.id_inter,mod.InstallForm.id_meter,
    mod.InstallForm.installer,mod.InstallForm.interv_date,mod.InstallForm.form_data).all()  
    df=pd.DataFrame(install)
    df=df.to_dict("records")
    def flatten_list(lst):
        return [
            {**item, **item.pop('form_data')}
            if 'form_data' in item
            else item
            for item in lst
        ]
    d=flatten_list(df)
    d=pd.DataFrame(d)
    d=d.loc[:,~d.columns.duplicated()]
    d=d[['assign_date','id_inter','id_meter','installer','interv_date',"Date d'intervention",'Etat du foyer','longitude','latitude','Type audio','Type tv',
    'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires "]]
    d["Type_intervention"]="Installation"
    print(d)
    d["Parking"]=d["Parking"].astype(float)
    d["Hotel"]=d["Hotel"].astype(float)
    d["Repas"]=d["Repas"].astype(float)
    d["Frais d'achat d'autres accessoires "]=d["Frais d'achat d'autres accessoires "].astype(float)
    d["Notes_Frais"]=d["Hotel"]+d["Repas"]+d["Parking"]+d["Frais d'achat d'autres accessoires "]
    d.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Distance':'distance','Hotel':'hotel','Parking':'parking','Type audio':'Type_audio','Type tv':'Type_tv',
    "Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Diagnostic intervention':'Diagnostic_intervention'
    },inplace=True)
    d.fillna(0,inplace=True)
    d=d.to_dict("records")
    print(d)
    return d
   
  


    

def get_uninstall_form(db: Session = next(get_db())):
    uninstall=db.query(mod.UninstallForm.assign_date,mod.UninstallForm.id_foyer,mod.UninstallForm.id_inter,mod.UninstallForm.id_meter,
    mod.UninstallForm.installer,mod.UninstallForm.interv_date,mod.UninstallForm.form_data).all() 
    df=pd.DataFrame(uninstall)
    df=df.to_dict("records")
    def flatten_list(lst):
        return [
            {**item, **item.pop('form_data')}
            if 'form_data' in item
            else item
            for item in lst
        ]
    d=flatten_list(df)
    d=pd.DataFrame(d)
    d=d.loc[:,~d.columns.duplicated()]
    d=d[['assign_date','id_foyer','id_inter','id_meter','installer','interv_date',"Date d'intervention",'Etat du foyer','longitude','latitude',
    'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires "
    #"'Frais d\'achat d\'autres accessoires'"
    ]]
    d["Type_intervention"]="Désinstallation"
    print(d)
    d["Parking"]=d["Parking"].astype(float)
    d["Hotel"]=d["Hotel"].astype(float)
    d["Repas"]=d["Repas"].astype(float)
    d["Frais d'achat d'autres accessoires "]=d["Frais d'achat d'autres accessoires "].astype(float)
    d["Notes_Frais"]=d["Hotel"]+d["Repas"]+d["Parking"]+d["Frais d'achat d'autres accessoires "]
    d.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Distance':'distance','Hotel':'hotel','Parking':'parking',
    "Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Diagnostic intervention':'Diagnostic_intervention'
    },inplace=True)
    d.fillna(0,inplace=True)
    d=d.to_dict("records")
    print(d)
    max_values_by_date_and_installer = {}

    for record in d:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in max_values_by_date_and_installer:
            max_values_by_date_and_installer[key] = {
                "Repas": float(record["Repas"]),
                "parking": float(record["parking"]),
                "hotel": float(record["hotel"])
            }
        else:
            max_values_by_date_and_installer[key]["Repas"] = max(max_values_by_date_and_installer[key]["Repas"], float(record["Repas"]))
            max_values_by_date_and_installer[key]["parking"] = max(max_values_by_date_and_installer[key]["parking"], float(record["parking"]))
            max_values_by_date_and_installer[key]["hotel"] = max(max_values_by_date_and_installer[key]["hotel"], float(record["hotel"]))

    # Mettre à jour les enregistrements avec les valeurs maximales pour chaque date d'intervention et chaque installateur.
    updated_records = []
    updated_keys = set()
    for record in d:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in updated_keys:
            record["Repas"] = float(max_values_by_date_and_installer[key]["Repas"])
            record["parking"] = float(max_values_by_date_and_installer[key]["parking"])
            record["hotel"] = float(max_values_by_date_and_installer[key]["hotel"])
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])

            updated_keys.add(key)
        else:
            record["Repas"] = 0
            record["parking"] = 0
            record["hotel"] = 0
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])

        updated_records.append(record)

    return updated_records
   


def get_uninstall_form_before_process(db: Session = next(get_db())):
    uninstall=db.query(mod.UninstallForm.assign_date,mod.UninstallForm.id_foyer,mod.UninstallForm.id_inter,mod.UninstallForm.id_meter,
    mod.UninstallForm.installer,mod.UninstallForm.interv_date,mod.UninstallForm.form_data).all() 
    df=pd.DataFrame(uninstall)
    df=df.to_dict("records")
    def flatten_list(lst):
        return [
            {**item, **item.pop('form_data')}
            if 'form_data' in item
            else item
            for item in lst
        ]
    d=flatten_list(df)
    d=pd.DataFrame(d)
    d=d.loc[:,~d.columns.duplicated()]
    d=d[['assign_date','id_foyer','id_inter','id_meter','installer','interv_date',"Date d'intervention",'Etat du foyer','longitude','latitude',
    'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires "
    #"'Frais d\'achat d\'autres accessoires'"
    ]]
    d["Type_intervention"]="Désinstallation"
    print(d)
    d["Parking"]=d["Parking"].astype(float)
    d["Hotel"]=d["Hotel"].astype(float)
    d["Repas"]=d["Repas"].astype(float)
    d["Frais d'achat d'autres accessoires "]=d["Frais d'achat d'autres accessoires "].astype(float)
    d["Notes_Frais"]=d["Hotel"]+d["Repas"]+d["Parking"]+d["Frais d'achat d'autres accessoires "]
    d.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Distance':'distance','Hotel':'hotel','Parking':'parking',
    "Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Diagnostic intervention':'Diagnostic_intervention'
    },inplace=True)
    d.fillna(0,inplace=True)
    d=d.to_dict("records")
    print(d)
    return d





def get_all_forms(db: Session = next(get_db())):
    #Get Install Form
    d_uninstall=[]
    d_install=[]
    d_repair=[]
    install=db.query(mod.InstallForm.assign_date,mod.InstallForm.id_inter,mod.InstallForm.id_meter,
    mod.InstallForm.installer,mod.InstallForm.interv_date,mod.InstallForm.form_data).all()  
    if install:
        df_install=pd.DataFrame(install)
        df_install=df_install.to_dict("records")
        def flatten_list(lst):
            return [
                {**item, **item.pop('form_data')}
                if 'form_data' in item
                else item
                for item in lst
            ]
        d_install=flatten_list(df_install)
        d_install=pd.DataFrame(d_install)
        d_install=d_install.loc[:,~d_install.columns.duplicated()]
        d_install=d_install[['assign_date','id_inter','id_meter','installer','interv_date',"Date d'intervention",'Etat du foyer','longitude','latitude','Type audio','Type tv',
        'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires "]]
        d_install["Type_intervention"]="Installation"
        print(d_install)
        d_install["Parking"]=d_install["Parking"].astype(float)
        d_install["Hotel"]=d_install["Hotel"].astype(float)
        d_install["Repas"]=d_install["Repas"].astype(float)
        d_install["Frais d'achat d'autres accessoires "]=d_install["Frais d'achat d'autres accessoires "].astype(float)
        d_install["Notes_Frais"]=d_install["Hotel"]+d_install["Repas"]+d_install["Parking"]+d_install["Frais d'achat d'autres accessoires "]
        d_install.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Distance':'distance','Hotel':'hotel','Parking':'parking','Type audio':'Type_audio','Type tv':'Type_tv',
        "Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Diagnostic intervention':'Diagnostic_intervention'
        },inplace=True)
        d_install.fillna(0,inplace=True)
        d_install=d_install.to_dict("records")

    

    
    #Get Repair Form
    repair=db.query(mod.RepareForm.assign_date,mod.RepareForm.id_foyer,mod.RepareForm.id_inter,mod.RepareForm.installer,
    mod.RepareForm.interv_date,mod.RepareForm.new_id_meter,mod.RepareForm.old_id_meter,mod.RepareForm.form_data).all()
    if repair:
        df_repair=pd.DataFrame(repair)
        df_repair=df_repair.to_dict("records")
        #df_json=pd.json_normalize(df["form_data"])
        #print(df_json)
        #df_concat=df.join(df_json)
        #print(df_concat)
        def flatten_list(lst):
            return [
                {**item, **item.pop('form_data')}
                if 'form_data' in item
                else item
                for item in lst
            ]
        d_repair=flatten_list(df_repair)
        d_repair=pd.DataFrame(d_repair)
        d_repair=d_repair.loc[:,~d_repair.columns.duplicated()]
        d_repair=d_repair[['assign_date','id_foyer','id_inter','installer','interv_date','new_id_meter','old_id_meter',"Date d'intervention",'Etat du foyer','Nouveau id_meter','Type audio','Type tv','longitude','latitude',
        'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires ",'Intervention telephonique']]
        d_repair["Type_intervention"]="Réparation"
        d_repair["Parking"]=d_repair["Parking"].astype(float)
        d_repair["Hotel"]=d_repair["Hotel"].astype(float)
        d_repair["Repas"]=d_repair["Repas"].astype(float)
        d_repair["Frais d'achat d'autres accessoires "]=d_repair["Frais d'achat d'autres accessoires "].astype(float)
        d_repair["Notes_Frais"]=d_repair["Hotel"]+d_repair["Repas"]+d_repair["Parking"]+d_repair["Frais d'achat d'autres accessoires "]
        #d["Distance"]=str(d["Distance"])
        d_repair.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Nouveau id_meter':'Nouveau_id_meter','Distance':'distance','Hotel':'hotel','Parking':'parking',
        'Type audio':'Type_audio','Type tv':'Type_tv','Diagnostic intervention':'Diagnostic_intervention',"Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Intervention telephonique':'Intervention_telephonique'
        },inplace=True)

        d_repair.fillna(0,inplace=True)
        d_repair=d_repair.to_dict("records")





    #Get Uninstall Form
    uninstall=db.query(mod.UninstallForm.assign_date,mod.UninstallForm.id_foyer,mod.UninstallForm.id_inter,mod.UninstallForm.id_meter,
    mod.UninstallForm.installer,mod.UninstallForm.interv_date,mod.UninstallForm.form_data).all() 
    if uninstall:
        df_uninstall=pd.DataFrame(uninstall)
        df_uninstall=df_uninstall.to_dict("records")
        def flatten_list(lst):
            return [
                {**item, **item.pop('form_data')}
                if 'form_data' in item
                else item
                for item in lst
            ]
        d_uninstall=flatten_list(df_uninstall)
        d_uninstall=pd.DataFrame(d_uninstall)
        d_uninstall=d_uninstall.loc[:,~d_uninstall.columns.duplicated()]
        d_uninstall=d_uninstall[['assign_date','id_foyer','id_inter','id_meter','installer','interv_date',"Date d'intervention",'Etat du foyer','longitude','latitude',
        'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires "
        #"'Frais d\'achat d\'autres accessoires'"
        ]]
        d_uninstall["Type_intervention"]="Désinstallation"
        print(d_uninstall)
        d_uninstall["Parking"]=d_uninstall["Parking"].astype(float)
        d_uninstall["Hotel"]=d_uninstall["Hotel"].astype(float)
        d_uninstall["Repas"]=d_uninstall["Repas"].astype(float)
        d_uninstall["Frais d'achat d'autres accessoires "]=d_uninstall["Frais d'achat d'autres accessoires "].astype(float)
        d_uninstall["Notes_Frais"]=d_uninstall["Hotel"]+d_uninstall["Repas"]+d_uninstall["Parking"]+d_uninstall["Frais d'achat d'autres accessoires "]
        d_uninstall.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Distance':'distance','Hotel':'hotel','Parking':'parking',
        "Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Diagnostic intervention':'Diagnostic_intervention'
        },inplace=True)
        d_uninstall.fillna(0,inplace=True)
        d_uninstall=d_uninstall.to_dict("records")
    

    d_merged=d_install+d_repair+d_uninstall
    max_values_by_date_and_installer = {}

    for record in d_merged:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in max_values_by_date_and_installer:
            max_values_by_date_and_installer[key] = {
                "Repas": float(record["Repas"]),
                "parking": float(record["parking"]),
                "hotel": float(record["hotel"])
            }
        else:
            max_values_by_date_and_installer[key]["Repas"] = max(max_values_by_date_and_installer[key]["Repas"], float(record["Repas"]))
            max_values_by_date_and_installer[key]["parking"] = max(max_values_by_date_and_installer[key]["parking"], float(record["parking"]))
            max_values_by_date_and_installer[key]["hotel"] = max(max_values_by_date_and_installer[key]["hotel"], float(record["hotel"]))

    # Mettre à jour les enregistrements avec les valeurs maximales pour chaque date d'intervention et chaque installateur.
    updated_records = []
    updated_keys = set()
    for record in d_merged:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in updated_keys:
            record["Repas"] = float(max_values_by_date_and_installer[key]["Repas"])
            record["parking"] = float(max_values_by_date_and_installer[key]["parking"])
            record["hotel"] = float(max_values_by_date_and_installer[key]["hotel"])
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])

            updated_keys.add(key)
        else:
            record["Repas"] = 0
            record["parking"] = 0
            record["hotel"] = 0
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])

        updated_records.append(record)

    return updated_records





def get_all_forms_by_date(date_deb,date_fin,db: Session = next(get_db())):
    #Get Install Form
    #updated_records = []
    d_uninstall=[]
    d_install=[]
    d_repair=[]
    install=db.query(mod.InstallForm.assign_date,mod.InstallForm.id_inter,mod.InstallForm.id_meter,
    mod.InstallForm.installer,mod.InstallForm.interv_date,mod.InstallForm.form_data).all()
    if install:
        df_install=pd.DataFrame(install)
        df_install=df_install.to_dict("records")
        def flatten_list(lst):
            return [
                {**item, **item.pop('form_data')}
                if 'form_data' in item
                else item
                for item in lst
            ]
        d_install=flatten_list(df_install)
        d_install=pd.DataFrame(d_install)
        d_install=d_install.loc[:,~d_install.columns.duplicated()]
        d_install=d_install[['assign_date','id_inter','id_meter','installer','interv_date',"Date d'intervention",'Etat du foyer','longitude','latitude','Type audio','Type tv',
        'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires "]]
        d_install["Type_intervention"]="Installation"
        print(d_install)
        d_install["Parking"]=d_install["Parking"].astype(float)
        d_install["Hotel"]=d_install["Hotel"].astype(float)
        d_install["Repas"]=d_install["Repas"].astype(float)
        d_install["Frais d'achat d'autres accessoires "]=d_install["Frais d'achat d'autres accessoires "].astype(float)
        d_install["Notes_Frais"]=d_install["Hotel"]+d_install["Repas"]+d_install["Parking"]+d_install["Frais d'achat d'autres accessoires "]
        d_install.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Distance':'distance','Hotel':'hotel','Parking':'parking','Type audio':'Type_audio','Type tv':'Type_tv',
        "Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Diagnostic intervention':'Diagnostic_intervention'
        },inplace=True)
        d_install.fillna(0,inplace=True)
        d_install=d_install.to_dict("records")
        #updated_records += d_install

    
    

    
    #Get Repair Form
    repair=db.query(mod.RepareForm.assign_date,mod.RepareForm.id_foyer,mod.RepareForm.id_inter,mod.RepareForm.installer,
    mod.RepareForm.interv_date,mod.RepareForm.new_id_meter,mod.RepareForm.old_id_meter,mod.RepareForm.form_data).all()
    if repair:
        df_repair=pd.DataFrame(repair)
        df_repair=df_repair.to_dict("records")
        #df_json=pd.json_normalize(df["form_data"])
        #print(df_json)
        #df_concat=df.join(df_json)
        #print(df_concat)
        def flatten_list(lst):
            return [
                {**item, **item.pop('form_data')}
                if 'form_data' in item
                else item
                for item in lst
            ]
        d_repair=flatten_list(df_repair)
        d_repair=pd.DataFrame(d_repair)
        d_repair=d_repair.loc[:,~d_repair.columns.duplicated()]
        d_repair=d_repair[['assign_date','id_foyer','id_inter','installer','interv_date','new_id_meter','old_id_meter',"Date d'intervention",'Etat du foyer','Nouveau id_meter','Type audio','Type tv','longitude','latitude',
        'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires ",'Intervention telephonique']]
        d_repair["Type_intervention"]="Réparation"
        d_repair["Parking"]=d_repair["Parking"].astype(float)
        d_repair["Hotel"]=d_repair["Hotel"].astype(float)  
        d_repair["Repas"]=d_repair["Repas"].astype(float)
        d_repair["Frais d'achat d'autres accessoires "]=d_repair["Frais d'achat d'autres accessoires "].astype(float)
        d_repair["Notes_Frais"]=d_repair["Hotel"]+d_repair["Repas"]+d_repair["Parking"]+d_repair["Frais d'achat d'autres accessoires "]
        #d["Distance"]=str(d["Distance"])
        d_repair.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Nouveau id_meter':'Nouveau_id_meter','Distance':'distance','Hotel':'hotel','Parking':'parking',
        'Type audio':'Type_audio','Type tv':'Type_tv','Diagnostic intervention':'Diagnostic_intervention',"Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Intervention telephonique':'Intervention_telephonique'
        },inplace=True)

        d_repair.fillna(0,inplace=True)
        d_repair=d_repair.to_dict("records")
        #updated_records += d_repair
        





    #Get Uninstall Form
    uninstall=db.query(mod.UninstallForm.assign_date,mod.UninstallForm.id_foyer,mod.UninstallForm.id_inter,mod.UninstallForm.id_meter,
    mod.UninstallForm.installer,mod.UninstallForm.interv_date,mod.UninstallForm.form_data).all()
    if uninstall:
        df_uninstall=pd.DataFrame(uninstall)
        df_uninstall=df_uninstall.to_dict("records")
        def flatten_list(lst):
            return [
                {**item, **item.pop('form_data')}
                if 'form_data' in item
                else item
                for item in lst
            ]
        d_uninstall=flatten_list(df_uninstall)
        d_uninstall=pd.DataFrame(d_uninstall)
        d_uninstall=d_uninstall.loc[:,~d_uninstall.columns.duplicated()]
        d_uninstall=d_uninstall[['assign_date','id_foyer','id_inter','id_meter','installer','interv_date',"Date d'intervention",'Etat du foyer','longitude','latitude',
        'Distance','Parking','Repas','Hotel','Diagnostic intervention',"Frais d'achat d'autres accessoires "
        #"'Frais d\'achat d\'autres accessoires'"
        ]]
        d_uninstall["Type_intervention"]="Désinstallation"
        print(d_uninstall)
        d_uninstall["Parking"]=d_uninstall["Parking"].astype(float)
        d_uninstall["Hotel"]=d_uninstall["Hotel"].astype(float)
        d_uninstall["Repas"]=d_uninstall["Repas"].astype(float)
        d_uninstall["Frais d'achat d'autres accessoires "]=d_uninstall["Frais d'achat d'autres accessoires "].astype(float)
        d_uninstall["Notes_Frais"]=d_uninstall["Hotel"]+d_uninstall["Repas"]+d_uninstall["Parking"]+d_uninstall["Frais d'achat d'autres accessoires "]
        d_uninstall.rename(columns={ "Date d'intervention":"Date_intervention","Etat du foyer":'Etat_du_foyer','Distance':'distance','Hotel':'hotel','Parking':'parking',
        "Frais d'achat d'autres accessoires ":'Frais_autres_accessoires','Diagnostic intervention':'Diagnostic_intervention'
        },inplace=True)
        d_uninstall.fillna(0,inplace=True)
        d_uninstall=d_uninstall.to_dict("records")
        #updated_records += d_uninstall
    
    

    d_merged=d_install+d_repair+d_uninstall
    


    filtered_records = [
        record for record in d_merged
        if date_deb <= record["interv_date"] <= date_fin
    ]


    max_values_by_date_and_installer = {}

    for record in filtered_records:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in max_values_by_date_and_installer:
            max_values_by_date_and_installer[key] = {
                "Repas": float(record["Repas"]),
                "parking": float(record["parking"]),
                "hotel": float(record["hotel"])
            }
        else:
            max_values_by_date_and_installer[key]["Repas"] = max(max_values_by_date_and_installer[key]["Repas"], float(record["Repas"]))
            max_values_by_date_and_installer[key]["parking"] = max(max_values_by_date_and_installer[key]["parking"], float(record["parking"]))
            max_values_by_date_and_installer[key]["hotel"] = max(max_values_by_date_and_installer[key]["hotel"], float(record["hotel"]))

    # Mettre à jour les enregistrements avec les valeurs maximales pour chaque date d'intervention et chaque installateur.
    updated_records = []
    updated_keys = set()
    for record in filtered_records:
        interv_date = record["interv_date"]
        key = (interv_date, record["installer"])

        if key not in updated_keys:
            record["Repas"] = float(max_values_by_date_and_installer[key]["Repas"])
            record["parking"] = float(max_values_by_date_and_installer[key]["parking"])
            record["hotel"] = float(max_values_by_date_and_installer[key]["hotel"])
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])

            updated_keys.add(key)
        else:
            record["Repas"] = 0
            record["parking"] = 0
            record["hotel"] = 0
            record["Notes_Frais"]=float(record["Repas"]+record["hotel"]+record["parking"]+record["Frais_autres_accessoires"])

        updated_records.append(record)

    return updated_records


   

#End Update


# new_columns=['assign_date', 'id_foyer', 'id_inter', 'installer', 'interv_date',
 #       'new_id_meter', 'old_id_meter', 'Date d'intervention',
 #       'Date d'intervention_comment', 'Etat du foyer', 'Etat du foyer_comment',
 #       'Nouveau id_meter', 'Nouveau id_meter_comment', 'Type audio',
 #       'Type audio_comment', 'Type tv', 'Type tv_comment', 'longitude',
 #       'longitude_comment', 'latitude', 'latitude_comment', 'distance',
 #       'distance_comment', 'parking', 'parking_comment', 'Frais de repas',
 #       'Frais de repas_comment', 'hotel', 'hotel_comment',
 #       'Frais d'achat d'autres accessoires',
 #       'Frais d'achat d'autres accessoires_comment', 'Distance',
 #       'Distance_comment', 'Parking', 'Parking_comment', 'Repas',
 #       'Repas_comment', 'Hotel', 'Hotel_comment',
 #       'Frais d'achat d'autres accessoires ',
 #       'Frais d'achat d'autres accessoires _comment',
 #       'Diagnostic intervention', 'Diagnostic intervention_comment',
 #       'Détails de l'intervention / Commentaires',
 #       'Détails de l'intervention / Commentaires_comment', 'TEST']



#     d=d.rename(columns={"assign_date":"assign_date","id_foyer":"id_foyer",'id_inter':'id_inter','installer':'installer','interv_date':'interv_date'
#     'new_id_meter':'new_id_meter','old_id_meter':'old_id_meter',"Date d'intervention":'Date_intervention_comment','Etat du foyer':"Etat_du_foyer"
#     'Etat du foyer_comment':'Etat_du_foyer_comment','Nouveau_id_meter':'Nouveau id_meter','Nouveau_id_meter_comment':'Nouveau_id_meter_comment',
#     'Type audio':'Type_audio','Type audio_comment':'Type_audio_comment','Type tv':'Type_tv','Type tv_comment':'Type_tv_comment','longitude':'longitude',
#     'longitude_comment':'longitude_comment','latitude':'latitude','latitude_comment':'latitude_comment','distance':'distance',
#     'distance_comment':'distance_comment','parking':'parking','parking_comment':'parking_comment','Frais de repas':'Frais_de_repas',
#     'Frais de repas_comment':'Frais_de_repas_comment','hotel':'hotel','hotel_comment':'hotel_comment',"Frais d'achat d'autres accessoires":'Frais_achat_autres_accessoires',
#     'Frais d'achat d'autres accessoires_comment':'Frais_achat_autres_accessoires_comment','Distance':'Distance','Distance_comment':'Distance_comment','Parking':'Parking',
#     'Repas':'Repas','Repas_comment':'Repas_comment','Hotel':'Hotel','Hotel_comment':'Hotel_comment','Frais d'achat d'autres accessoires ':'Frais_achat_autres_accessoire',
#     'Frais d'achat d'autres accessoires _comment':'Frais_achat_autres_accessoire_comment','Diagnostic intervention':'Diagnostic_intervention',
#     'Diagnostic intervention_comment':'Diagnostic_intervention_comment','Détails de l'intervention / Commentaires':'Détails_intervention_Commentaires',
#     "Détails de l'intervention / Commentaires_comment":'Détails_intervention_Commentaires_comment','TEST':'TEST' "})







################################################################# Get Number of Interventions #####################################################################
def get_interventions_count(date_deb: date, date_fin: date, db: Session = next(get_db())):
    repair=db.query(mod.RepareForm).filter(mod.RepareForm.interv_date.between(date_deb, date_fin)).all()
    df_repair=pd.DataFrame(repair)
    count_repair=df_repair.shape[0]

    install=db.query(mod.InstallForm).filter(mod.InstallForm.interv_date.between(date_deb, date_fin)).all()
    df_install=pd.DataFrame(install)
    count_install=df_install.shape[0]

    uninstall=db.query(mod.UninstallForm).filter(mod.UninstallForm.interv_date.between(date_deb, date_fin)).all()
    df_uninstall=pd.DataFrame(uninstall)
    count_uninstall=df_uninstall.shape[0]

   # result = {"Count Repair": count_repair, "Count Install": count_install, "Count Uninstall": count_uninstall, "Total Count": count_repair+count_install+count_uninstall}


    

    result=[
    {
        "type_intervention": "Réparation",
        "count": count_repair
    },
    {
        "type_intervention": "Désinstallation",
        "count": count_uninstall
    },
    {
        "type_intervention": "Installation",
        "count": count_install
    },
    {
        "type_intervention": "Intervention téléphonique",
        "count": 0
    }
        ]
    print(result)
    return result


################################################################# Get Number of Interventions By Installer #####################################################################
def get_interventions_count_by_installer(installer:str,date_deb: date, date_fin: date, db: Session = next(get_db())):
    repair=db.query(mod.RepareForm).filter(mod.RepareForm.installer==installer,mod.RepareForm.interv_date.between(date_deb, date_fin)).all()
    df_repair=pd.DataFrame(repair)
    count_repair=df_repair.shape[0]

    install=db.query(mod.InstallForm).filter(mod.InstallForm.installer==installer,mod.InstallForm.interv_date.between(date_deb, date_fin)).all()
    df_install=pd.DataFrame(install)
    count_install=df_install.shape[0]

    uninstall=db.query(mod.UninstallForm).filter(mod.UninstallForm.installer==installer,mod.UninstallForm.interv_date.between(date_deb, date_fin)).all()
    df_uninstall=pd.DataFrame(uninstall)
    count_uninstall=df_uninstall.shape[0]

   # result = {"Count Repair": count_repair, "Count Install": count_install, "Count Uninstall": count_uninstall, "Total Count": count_repair+count_install+count_uninstall}


    

    result=[
  {
    "type_intervention": "Réparation",
    "count": count_repair
  },
  {
    "type_intervention": "Désinstallation",
    "count": count_uninstall
  },
  {
    "type_intervention": "Installation",
    "count": count_install
  },
  {
    "type_intervention": "Intervention téléphonique",
    "count": 0
  }
    ]








    
    print(result)
    return result
    


    #uninstall=db.query(mod.UninstallForm).all() 
    #install=db.query(mod.InstallForm).all() 

    #df_repair=pd.DataFrame(repair)
   
    #df_repair['interv_date'] = pd.to_datetime(df_repair['interv_date'])
    #df_repair['month'] = df_repair['interv_date'].dt.month
    #count_per_month = df_repair['month'].value_counts().sort_index()
    #df_install=pd.DataFrame(install)
   # df_uninstall=pd.DataFrame(uninstall)
    #count=df_repair.shape[0]+df_install.shape[0]+df_uninstall.shape[0]
   # print(count)
    #return count




   

 
