
from sqlalchemy import ARRAY, Column, Date, DateTime, Float, ForeignKey, Integer, JSON, String, text
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()




class SelectedInterventions(Base):
    __tablename__ = 'selected_interventions'

    id_inter = Column( primary_key=True, comment='concate date/id_foyer')
    id_foyer = Column(Integer)
    id_meter = Column(String)
    assign_date = Column(DateTime)
    installer = Column(String)
    inter_type = Column(Integer)
    inter_status = Column(Integer)
    prob = Column(ARRAY(String))
    ville= Column(String)
    latitude= Column(Float)
    longitude= Column(Float)
    adresse= Column(String)
    nom= Column(String)
    telephones= Column(ARRAY(String))
    Numero_recrutement = Column(String)



class ValidatedInterventions(Base):
    __tablename__ = 'validated_interventions'

    id_inter =Column( primary_key=True)
    statut_intervention =Column(String)
    Type_intervention =Column(String)
    id_foyer =Column(Integer)
    #old_id_meter =Column(String)
    id_meter =Column(String)
    new_id_meter =Column(String)
    interv_date = Column(DateTime)
    assign_date = Column(DateTime)
    installer = Column(String)
    Etat_du_foyer = Column(String)
    #Updated 18/08/2023
    Intervention_telephonique = Column(String)
    #Nouveau_id_meter = Column(String)
    Type_audio = Column(String)
    Type_tv = Column(String)
    Diagnostic_intervention = Column(String)
    longitude = Column(String)
    latitude = Column(String)
    distance = Column(String)
    parking = Column(String)
    Repas = Column(String)
    hotel = Column(String)
    accessoires = Column(String)
    Notes_Frais=Column(Integer)


class BusinessValidationInterventions(Base):
    __tablename__ = 'Business_Validation_Interventions'

    id_inter =Column( primary_key=True)
    statut_intervention =Column(String)
    id_foyer =Column(Integer)
    #old_id_meter =Column(String)
    new_id_meter =Column(String)
    id_meter =Column(String)
    interv_date = Column(DateTime)
    assign_date = Column(DateTime)
    installer = Column(String)
    Etat_du_foyer = Column(String)
    #Updated 18/08/2023
    Intervention_telephonique = Column(String)
    #Nouveau_id_meter = Column(String)
    Type_audio = Column(String)
    Type_intervention = Column(String)
    Type_tv = Column(String)
    Diagnostic_intervention = Column(String)
    longitude = Column(String)
    latitude = Column(String)
    #distance = Column(String)
    #parking = Column(String)
    #Repas = Column(String)
    #hotel = Column(String)
    #Notes_Frais=Column(String)
    
    # initial_workspace = relationship('InitialWorkspace', back_populates='selected_interventions')
    #ForeignKey('repare_form.id_inter'), ForeignKey('uninstall_form.id_inter'), ForeignKey('install_form.id_inter'),


class Primes(Base):
    __tablename__ = 'Primes'
    id_inter =Column( primary_key=True)
    Foyer_ID =Column(Integer)
    id_meter  =Column(String)
    new_id_meter = Column(String)
    Jour =Column(Integer)
    coefficient=Column(Integer)
    prix_coeff=Column(Float)
    prime_intervention=Column(Float)
    Nbr_OK=Column(Integer)
    validation= Column(String)
    validation_finale= Column(String)
    installer = Column(String)
    Date_intervention=Column(Date)
    Type_intervention= Column(String)
    Intervention_telephonique= Column(String)

    class Config:
	    orm_mode=True
