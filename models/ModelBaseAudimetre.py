from sqlalchemy import Column, ForeignKey, Integer, String,DateTime,Identity
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Date, ForeignKey, Integer, JSON, String, text,Identity
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()







class AudimetreInstallateur(Base):

    __tablename__ = 'audimetre_installateur'

    __table_args__ = {
        'comment': 'la table audimetre_installateur qui contient les donn��es des audimetres chez les installateurs '
    }

    id_meter = Column(ForeignKey('audimetre.id_meter', ondelete='CASCADE', onupdate='CASCADE'), primary_key=True,comment="cl�� primaire provenant de la table audimetre")
    nom_installateur =Column(ForeignKey('installateurs.nom_installateur', ondelete='CASCADE', onupdate='CASCADE'), nullable=False, comment="cl�� ��trang��re provenant de la table installateurs")
    
    
