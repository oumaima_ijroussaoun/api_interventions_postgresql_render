from sqlalchemy import Column, Date, ForeignKey, Integer, JSON, String, text,Identity
from sqlalchemy.orm import declarative_base, relationship

Base = declarative_base()




class InstallForm(Base):
    __tablename__ = 'install_form'

    #id_inter = Column(Integer, Identity(start=1), primary_key=True)
    id_inter = Column(String, primary_key=True, unique=True, nullable=False,
                      server_default=text("get_next_install_id_inter()"),
                      server_onupdate=text("''"))

    id_meter = Column(String)
    interv_date = Column(Date)
    assign_date = Column(Date)
    Numero_recrutement = Column(String)
    installer = Column(String)
    form_data = Column(JSON)


class RepareForm(Base):
    __tablename__ = 'repare_form'

    id_inter = Column(String, primary_key=True, unique=True, nullable=False,
                      server_default=text("get_next_repair_id_inter()"),
                      server_onupdate=text("''"))
    id_foyer = Column(Integer)
    old_id_meter = Column(String)
    new_id_meter = Column(String)

    interv_date = Column(Date)
    assign_date = Column(Date)
    installer = Column(String)
    form_data = Column(JSON)


class UninstallForm(Base):
    __tablename__ = 'uninstall_form'

    id_inter = Column(String, primary_key=True, unique=True, nullable=False,
                      server_default=text("get_next_uninstall_id_inter()"),
                      server_onupdate=text("''"))
    id_foyer = Column(Integer)
    id_meter = Column(String)
    interv_date = Column(Date)
    assign_date = Column(Date)
    installer = Column(String)
    form_data = Column(JSON)
