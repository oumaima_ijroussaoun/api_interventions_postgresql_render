
def include_route(app):
    
    app.include_router(forms.route)
    app.include_router(gpanel.route)
    # app.include_router(init_workspace.routeInit)
    app.include_router(interventions.routeInter)
