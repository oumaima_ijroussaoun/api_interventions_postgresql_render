
from fastapi import APIRouter, Depends,HTTPException,status
from datetime import date, datetime, timedelta
from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
from typing import List
from schemas import interventions as schem
from crud import interventions




routeInter = APIRouter(prefix="/Interventions",tags=["Interventions"])




############### Post Validated Interventions #################
@routeInter.post("/post/validatedInterventions/")
def validated_intervetions_post(data:List[dict]):
    
    
    return interventions.post_validated_interventions(data)





############### Post Primes #################
@routeInter.post("/post/primes/")
def primes_post(data:List[dict],installer):
    
    
    return interventions.post_primes(data,installer)




############### Get Validated Interventions #################
@routeInter.get("/get/validatedInterventions/")
def validated_intervetions_get():
    
    
    return interventions.get_validated_interventions()



############### Get Validated Interventions by date #################
@routeInter.get("/get/validatedInterventions/by_date")
def validated_intervetions_get_by_date(date_deb:date,date_fin:date):
    
    
    return interventions.get_validated_interventions_by_date(date_deb,date_fin)



############### Get Primes #################
@routeInter.get("/get/primes")
def primes_get():
    
    
    return interventions.get_primes()





############### Get Primes by installer and date #################
@routeInter.get("/get/primes/by/installer")
def primes_get_by_installer(installer:str,date_deb:date,date_fin:date):
    
    
    return interventions.get_primes_by_installer(installer,date_deb,date_fin)

############### Get Primes by type d'intervention #################
@routeInter.get("/get/primes/by/type_intervention")
def primes_by_type_intervention_get(date_deb:date,date_fin:date):
    
    
    return interventions.get_primes_by_type_intervention(date_deb,date_fin)


############### Get Primes by type d'intervention and installer #################
@routeInter.get("/get/primes/by/type_intervention_installer")
def primes_by_type_intervention_installer_get(installer:str,date_deb:date,date_fin:date):
    
    
    return interventions.get_primes_by_type_intervention_installer(installer,date_deb,date_fin)



############### Get Validated Interventions By Installer #################
@routeInter.get("/get/validatedInterventions/ByInstaller")
def validated_interventions_by_installer_get(installer:str,date_deb:date,date_fin:date):
    
    
    return interventions.get_validated_interventions_by_installer(installer,date_deb,date_fin)



############### Get Validated Interventions By Installer after process(repas,parking,hotel)#################
@routeInter.get("/get/validatedInterventions/ByInstaller/AfterProcess")
def validated_interventions_by_installer_process_get(installer:str,date_deb:date,date_fin:date):
    
    
    return interventions.get_validated_interventions_by_installer_process(installer,date_deb,date_fin)




############### Get Global Repondant Foyers #################
@routeInter.get("/get/Global_Repondant/Foyers")
def Global_Repondant_Foyers_get(installer:str,date_deb:date,date_fin:date,nbr_jour_apres_interv:int):
    
    
    return interventions.Global_Repondant_Foyers(installer,date_deb,date_fin,nbr_jour_apres_interv)

############### Get Global Repondant Foyers #################
@routeInter.get("/get/Global_Repondant/Foyers/By/Type_intervention")
def Global_Repondant_Foyers_By_Type_intervention_get(installer:str,date_deb:date,date_fin:date,nbr_jour_apres_interv:int):
    
    
    return interventions.Global_Repondant_Foyers_By_Type_intervention(installer,date_deb,date_fin,nbr_jour_apres_interv)


############### Get Business Validation Interventions #################
@routeInter.get("/get/BusinessValidationInterventions")
def Business_Validation_Intervetions_get():
    
    
    return interventions.get_Business_Validation_Interventions()





############### Get Business Validation Interventions By date #################
@routeInter.get("/get/BusinessValidationInterventions/by_date")
def Business_Validation_Intervetions_get_by_date(date_deb:date,date_fin:date):
    
    
    return interventions.get_Business_Validation_Interventions_by_date(date_deb,date_fin)


############### Get Business Validation Interventions (Validated and Rework) #################
@routeInter.get("/get/BusinessValidationInterventions/Validated_Rework")
def Business_Validation_Interventions_Validated_Rework_get():
    
    
    return interventions.get_Business_Validation_Interventions_Validated_Rework()


############### Get Business Validation Interventions (Validated and Rework) By date #################
@routeInter.get("/get/BusinessValidationInterventions/Validated_Rework/by_date")
def Business_Validation_Interventions_Validated_Rework_get_by_date(date_deb:date,date_fin:date):
    
    
    return interventions.get_Business_Validation_Interventions_Validated_Rework_by_date(date_deb,date_fin)




############### Post Business Validation  Interventions #################
@routeInter.post("/post/BusinessValidationInterventions")
def Business_Validation_Intervetions_post(data:List[dict],statut_intervention):
    
    
    return interventions.post_Business_Validation_Interventions(data,statut_intervention)


############### Update  Interventions #################
@routeInter.put("/put/BusinessValidationInterventions")
def Business_Validation_Intervetions_put(data:dict):
    
    
    return interventions.put_Business_Validation_Interventions(data)




@routeInter.post("/post/")
def intervetions_post(data:List[dict]):
    
    
    return interventions.post_interventions(data)



############################ Post Desinstallations #############################
@routeInter.post("/post/desinstallations")
def desinstallations_post(data:List[dict]):
    
    
    return interventions.post_desinstallations(data)
###############################################################################


############################ Post Desinstallations #############################
@routeInter.post("/post/installations")
def installations_post(data:List[dict]):
    
    
    return interventions.post_installations(data)
###############################################################################


@routeInter.get("/get/meters")
def intervetions_meters():
    return [x.id_meter for x in interventions.get_interventions_meters()]

@routeInter.get("/get/to_do")
def intervetions_to_do():

    return interventions.get_interventions()

@routeInter.get("/get/to_do/by_installer/{installer}")
def intervetions_to_do_by_installer(installer):

    return interventions.get_interventions_by_installer(installer)


#Update 15/09/2023 ####################################################################
@routeInter.get("/get/to_do/by_all/{installer}")
def intervetions_to_do_by_all(installer):

    return interventions.get_interventions_by_all(installer)
########################################################################################


@routeInter.get("/get/to_do/by_installer_type/{installer}/{type_inter}")
def intervetions_to_do_by_installer_type(installer,type_inter):

    return interventions.get_interventions_by_installer_type(installer,type_inter)


################### Updated 16/04/2024
@routeInter.get("/get/to_do/by_installer_ville/{installer}/{ville}")
def interventions_by_installer_by_ville(installer,ville):

    return interventions.get_interventions_by_installer_by_ville(installer,ville)

######## Get installations by installer and by ville
@routeInter.get("/get/installations_to_do/by_installer_ville/{installer}/{ville}")
def installations_by_installer_by_ville(installer,ville):

    return interventions.get_installations_by_installer_by_ville(installer,ville)

######## Get réparations by installer and by ville
@routeInter.get("/get/reparations_to_do/by_installer_ville/{installer}/{ville}")
def reparations_by_installer_by_ville(installer,ville):

    return interventions.get_reparations_by_installer_by_ville(installer,ville)

######## Get désinstallations by installer and by ville
@routeInter.get("/get/desinstallations_to_do/by_installer_ville/{installer}/{ville}")
def desinstallations_by_installer_by_ville(installer,ville):

    return interventions.get_desinstallations_by_installer_by_ville(installer,ville)










#Update 15/09/2023 ####################################################################
@routeInter.get("/get/to_do/by_all_type/{installer}/{type_inter}")
def intervetions_to_do_by_all_type(installer,type_inter):

    return interventions.get_interventions_by_all_type(installer,type_inter)
########################################################################################



@routeInter.get("/get/count/by_installer/{installer}")
def intervetions_count_by_installer(installer):

    return interventions.get_count_inter_by_installer(installer=installer)


#Update 15/09/2023 ####################################################################
@routeInter.get("/get/count/by_all/{installer}")
def intervetions_count_by_all(installer):

    return interventions.get_count_inter_by_all(installer=installer)
########################################################################################


@routeInter.get("/get/count/by_installer/by_ville/{installer}")
def intervetions_count_by_installer_by_ville(installer):

    return interventions.get_count_inter_by_installer_by_vile(installer=installer)



#Update 15/09/2023 ####################################################################
@routeInter.get("/get/count/by_all/by_ville/{installer}")
def intervetions_count_by_all_by_ville(installer):

    return interventions.get_count_inter_by_all_by_vile(installer=installer)
########################################################################################



@routeInter.get("/get/count/by_installer/stauts/{installer}")
def intervetions_count_by_installer_stauts(installer):

    return interventions.get_count_stats_by_installer(installer=installer)


#Update 15/09/2023 ####################################################################
@routeInter.get("/get/count/by_all/stauts/{installer}")
def intervetions_count_by_all_stauts(installer):

    return interventions.get_count_stats_by_all(installer=installer)
########################################################################################
