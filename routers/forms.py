from fastapi import APIRouter, Depends,HTTPException,status
from datetime import date, datetime, timedelta

from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
from typing import List
from schemas import forms as schem
from crud import forms as crud



route = APIRouter(prefix="/forms",tags=["forms"])

@route.post("/install/form") 
def insert_install_form(data:schem.InstallForm):
    try:
        return crud.post_install_form(data)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies")    

@route.post("/uninstall/form") 
def insert_uninstall_form(data:schem.UninstallForm):
    try:
        return crud.post_uninstall_form(data)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies") 

@route.post("/repaire/form") 
def insert_repaire_form(data:schem.RepareForm):
    try:
        return crud.post_repair_form(data)
    except:
        raise 
    # HTTPException(status_code=400, detail="verifier les données saisies") 

#Updated by Oumaima
@route.get("/repair/form/before_process")
def get_repair_form_before_process():

    return crud.get_repair_form_before_process()


@route.get("/repair/form")
def get_repair_form():

    return crud.get_repair_form()

@route.get("/install/form")
def get_install_form():

    return crud.get_install_form()

@route.get("/install/form/before_process")
def get_install_form_before_process():

    return crud.get_install_form_before_process()

@route.get("/uninstall/form")
def get_uninstall_form():

    return crud.get_uninstall_form()


@route.get("/uninstall/form/before_process")
def get_uninstall_form_before_process():

    return crud.get_uninstall_form_before_process()

#Get all forms
@route.get("/all/forms")
def get_all_forms():

    return crud.get_all_forms()



#Get all forms By date
@route.get("/all/forms/by_date")
def get_all_forms_by_date(date_deb:date,date_fin:date):

    return crud.get_all_forms_by_date(date_deb,date_fin)



@route.get("/interventions/count")
def get_interventions_count(date_deb:date,date_fin:date):

    return crud.get_interventions_count(date_deb,date_fin)  


@route.get("/interventions/count/by_installer")
def get_interventions_count_by_installer(installer:str,date_deb:date,date_fin:date):

    return crud.get_interventions_count_by_installer(installer,date_deb,date_fin) 


    
#End Update


