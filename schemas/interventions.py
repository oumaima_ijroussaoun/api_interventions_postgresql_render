from pydantic import BaseModel, Json
from typing import Optional,List,Union
import datetime as d

from pydantic.utils import Obj



class CreateInterventions(BaseModel):
    
   
    id_foyer : int
    id_meter : str
    assign_date:d.date
    inter_type:int
    installer: str
    inter_status:int
    prob:List[str]
    ville: str
    latitude:float
    longitude:float
    adresse: str
    nom: str
    telephones:list
    Numero_recrutement:str
    class Config:
	    orm_mode=True

class ValidatedInterventions(BaseModel):

    id_inter :str
    statut_intervention :str
    id_foyer :int
    id_meter :str
    new_id_meter :str
    interv_date :d.datetime
    assign_date :d.datetime
    installer :str
    Etat_du_foyer:str
    #Updated 18/08/2023
    Intervention_telephonique:str
    #Nouveau_id_meter:str
    Type_audio:str
    Type_tv:str
    Diagnostic_intervention:str
    longitude:str
    latitude:str
    distance:str
    parking:str
    Repas:str
    hotel:str
    accessoires:str
    Notes_Frais:int
    class Config:
	    orm_mode=True


    
class BusinessValidationInterventions(BaseModel):

    id_inter :str
    statut_intervention :str
    Type_intervention :str
    id_foyer :int
    id_meter :Union[str , None]
    new_id_meter :str
    interv_date :d.datetime
    assign_date :d.datetime
    installer :str
    Etat_du_foyer:str
    #Updated 18/08/2023
    Intervention_telephonique:str
    #Nouveau_id_meter:str
    Type_audio:str
    Type_tv:str
    Diagnostic_intervention:Union[str , None]
    longitude:str
    latitude:str
    #distance:str
    #parking:str
    #Repas:str
    #hotel:str
    #Notes_Frais:str
    class Config:
	    orm_mode=True


class Primes(BaseModel):

    id_inter :str
    Foyer_ID :int
    id_meter :str
    new_id_meter :str
    Jour :int
    coefficient:int
    prix_coeff:float
    prime_intervention:float
    Nbr_OK:int
    validation:str
    validation_finale:str
    installer :str
    Date_intervention:d.date
    Type_intervention:str
    Intervention_telephonique:str
    class Config:
	    orm_mode=True
