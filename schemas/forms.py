
import json
from pydantic import BaseModel
from typing import Optional
import datetime as d




class InstallForm(BaseModel):
   
    #id_inter :int
    id_meter :str
    interv_date :d.datetime
    assign_date :d.datetime
    Numero_recrutement :str
    installer :str
    form_data :dict
    #Type_intervention:str
    class Config:
	    orm_mode=True


class RepareForm(BaseModel):
   
    #id_inter :int
    id_foyer :int
    old_id_meter :str
    new_id_meter :str
    interv_date :d.datetime
    assign_date :d.datetime
    installer :str
    #Date_intervention:d.datetime
    #Etat_du_foyer:str
    #Nouveau_id_meter:str
    #Type_audio:str
    #Type_tv:str
    #Diagnostic_intervention:str
    #longitude:str
    #latitude:str
    #distance:str
    #parking:int
    #Repas:str
    #hotel:str
    #Type_intervention:str
    form_data :dict
    class Config:
	    orm_mode=True

class UninstallForm(BaseModel):
   
    
    id_foyer :int
    id_meter :str
    interv_date :d.datetime
    assign_date :d.datetime
    installer :str
    form_data :dict
    #id_inter :int
    #Date_intervention:d.datetime
    #Etat_du_foyer:str
    #Nouveau_id_meter:str
    #Type_audio:str
    #Type_tv:str
    #longitude:str
    #latitude:str
    #distance:str
    #parking:int
    #Frais_de_repas:str
    #Frais_achat_autres_accessoires: str
    #hotel:str
    #Type_intervention:str

    
    class Config:
	    orm_mode=True




